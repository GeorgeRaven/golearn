# every double comment will be used to auto generate help messages

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: all
all: tidy style test clean ## Run the program and its full suite of tests

.PHONY: tidy
tidy: ## Tidy up module dependencies and module file
	go mod tidy

.PHONY: style
style: ## Ensure go files are of the propper format
	gofmt -s -w -l .

.PHONY: test
test: ## Test each and every avaliable module in this tree
	go test $(go list ./... | grep -v /vendor/)

.PHONY: clean
clean:
