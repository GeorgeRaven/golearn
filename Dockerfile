FROM alpine:latest

# directory containing source files with respect to dockerfile
ARG SRCDIR="."
# directory where the source will exist in the dockerfile
ARG PKGDIR="/app"

RUN apk add --update --no-cache go

COPY ${SRCDIR} ${PKGDIR}
