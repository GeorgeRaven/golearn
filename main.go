package main

import (
	"log"
	"net/http"
)

func main() {
	address := ":4000"
	mux := http.NewServeMux()
	mux.HandleFunc("/", root)

	log.Printf("Starting new example server on %v", address)
	err := http.ListenAndServe(address, mux)
	log.Fatal(err)
}

func root(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello myself"))
}
